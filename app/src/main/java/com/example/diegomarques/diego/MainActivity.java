package com.example.diegomarques.diego;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;

import android.support.v7.app.AppCompatActivity;
import android.webkit.WebView;
import android.webkit.WebViewClient;



public class MainActivity extends AppCompatActivity  {

    WebView webview;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        webview = (WebView) findViewById(R.id.webview);
        webView();
    }

    //Metodo para lidar com o WebView
    private void webView() {


        //Habilitar JavaScript
        webview.getSettings().setJavaScriptEnabled(true);

        //Lidando com navegação da pagina
        webview.setWebViewClient(new MyWebViewClient());

        //carregando a URL no WebView
//        webview.loadUrl("file:///android_asset/teste.html");
        webview.loadUrl("http://clientez.gcodetec.com/");

    }

    // Metodo metodo de navegação para historico de paginas
    public void onBackPressed() {
        if (webview.canGoBack()) {
            webview.goBack();
        } else super.onBackPressed();
    }

    // Subclase WebViewClient() para lidar com a navegação das paginas
    private class MyWebViewClient extends WebViewClient {
        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            view.loadUrl(url);
            return true;
        }
    }
}